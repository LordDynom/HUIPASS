from PIL import Image

for i in range(0, 6565):
    col = Image.open("Frame"+str(i)+".jpg")
    gray = col.convert("L")
    bw = gray.point(lambda x: 0 if x<128 else 255, "1")
    bw.save("greyFrame"+str(i)+".png")
