#ifndef ETHERNETSHIELD_HPP
#define ETHERNETSHIELD_HPP

#include "due-spi.hpp"
#include "register_map.hpp"

using namespace due;
namespace target = hwlib::target;

class EthernetShield
{
private:
	spi_bus_due bus;

	const uint8_t baseGatewayAddress[4] 	= {0xC0, 0xA8, 0x01, 0x01};
	const uint8_t baseSubnetMask[4] 		= {0xFF, 0xFF, 0xFF, 0x00};
	const uint8_t baseSourceHWAddress[6] 	= {0xDE, 0xAD, 0xC0, 0xFF, 0xEE, 0x42};
	const uint8_t baseSourceIPAddress[4] 	= {0xC0, 0xA8, 0x01, 0x8E};
	
	uint16_t SOCKETMASK = 0x07FF;
	uint16_t RXRECVSIZE = 0x0000;
	
	uint8_t recvBuffer[0x07FF] = {0};
	
	//uint16_t socketOffset = 0x0000;
	//uint16_t 
	
	void write5100(const uint8_t registerSet, const uint8_t registerName, const uint8_t value) {
		uint8_t write_op[4] = {WRITE, registerSet, registerName, value};
		
		bus.write_and_read(hwlib::pin_out_dummy, sizeof(write_op), write_op, nullptr);
	}
	
	uint8_t read5100(const uint8_t registerSet, const uint8_t registerName) {
		uint8_t read_op[4] = {READ, registerSet, registerName, 0x00};
		uint8_t data_in[4] = {0};
		
		bus.write_and_read(hwlib::pin_out_dummy, sizeof(read_op), read_op, data_in);
		
		return data_in[3];
	}
	
	void largeRead(uint16_t addr, uint16_t len, uint8_t *buf) {
		uint8_t addr1, addr2;
		addr1 = addr >> 8;
		addr2 = addr & 0xFF;
		for(uint16_t i = 0; i < len; ++i) {
			buf[i] = read5100(addr1, addr2);
			hwlib::cout << "[+] Large read: " << hwlib::hex << buf[i] << hwlib::endl;
		}
	}
	
	void largeWrite(uint16_t addr, uint16_t len, uint8_t *data) {
		uint8_t addr1, addr2;
		addr1 = addr >> 8;
		addr2 = addr & 0xFF;
		for(uint16_t i = 0; i < len; ++i) {
			write5100(addr1, addr2, data[i]);
			hwlib::cout << "[+] Large write: " << hwlib::hex << data[i] << hwlib::endl;
		}
	}
	
	uint16_t concatBytes(uint8_t b1, uint8_t b2) {
		return b1 << 8 | b2;
	}
	
	void writeGatewayAddress() {
		write5100(COMMON_REGS, GWIPREG1, baseGatewayAddress[0]);
		write5100(COMMON_REGS, GWIPREG2, baseGatewayAddress[1]);
		write5100(COMMON_REGS, GWIPREG3, baseGatewayAddress[2]);
		write5100(COMMON_REGS, GWIPREG4, baseGatewayAddress[3]);
		
		hwlib::cout << "[+] Gateway Address written " 
		<< hwlib::dec << baseGatewayAddress[0] << "." << hwlib::dec << baseGatewayAddress[1] << "." << hwlib::dec << baseGatewayAddress[2] << "." << hwlib::dec << baseGatewayAddress[3] 
		<< hwlib::endl;
	}
	
	void writeSubnetMask() {
		write5100(COMMON_REGS, SUBNMREG1, baseSubnetMask[0]);
		write5100(COMMON_REGS, SUBNMREG2, baseSubnetMask[1]);
		write5100(COMMON_REGS, SUBNMREG3, baseSubnetMask[2]);
		write5100(COMMON_REGS, SUBNMREG4, baseSubnetMask[3]);
		
		hwlib::cout << "[+] Subnet mask written " 
		<< hwlib::dec << baseSubnetMask[0] << "." << hwlib::dec << baseSubnetMask[1] << "." << hwlib::dec << baseSubnetMask[2] << "." << hwlib::dec << baseSubnetMask[3] 
		<< hwlib::endl;
	}
	
	void writeSourceHWAddress() {
		write5100(COMMON_REGS, SHWADREG1, baseSourceHWAddress[0]);
		write5100(COMMON_REGS, SHWADREG2, baseSourceHWAddress[1]);
		write5100(COMMON_REGS, SHWADREG3, baseSourceHWAddress[2]);
		write5100(COMMON_REGS, SHWADREG4, baseSourceHWAddress[3]);
		write5100(COMMON_REGS, SHWADREG5, baseSourceHWAddress[4]);
		write5100(COMMON_REGS, SHWADREG6, baseSourceHWAddress[5]);
		
		hwlib::cout << "[+] Source hardware address written " 
		<< hwlib::hex << baseSourceHWAddress[0] << ":" << hwlib::hex << baseSourceHWAddress[1] << ":" << hwlib::hex << baseSourceHWAddress[2] 
		<< ":" << hwlib::hex << baseSourceHWAddress[3] << ":" << hwlib::hex << baseSourceHWAddress[4] << ":" << hwlib::hex << baseSourceHWAddress[5]
		<< hwlib::endl;
	}
	
	void writeSourceIPAddress() {
		write5100(COMMON_REGS, SIPADREG1, baseSourceIPAddress[0]);
		write5100(COMMON_REGS, SIPADREG2, baseSourceIPAddress[1]);
		write5100(COMMON_REGS, SIPADREG3, baseSourceIPAddress[2]);
		write5100(COMMON_REGS, SIPADREG4, baseSourceIPAddress[3]);
		
		hwlib::cout << "[+] Source IP written " 
		<< hwlib::dec << baseSourceIPAddress[0] << "." << hwlib::dec << baseSourceIPAddress[1] << "." << hwlib::dec << baseSourceIPAddress[2] << "." << hwlib::dec << baseSourceIPAddress[3] 
		<< hwlib::endl;
	}
	
	void enableInterrupt() {
		write5100(COMMON_REGS, IMRREG, 0xEF);
		
		hwlib::cout << "[*] Interrupts enabled" << hwlib::endl;
	}
	
	void setupRXMem() {
		write5100(COMMON_REGS, RXMEMSIZEREG, 0x55);
		
		hwlib::cout << "[+] RX memory setup" << hwlib::endl;
	}
	
	void setupTXMem() {
		write5100(COMMON_REGS, TXMEMSIZEREG, 0x55);
		
		hwlib::cout << "[+] TX memory setup" << hwlib::endl;
	}
	
	void increaseReadPointer(uint16_t size) {
		/*write5100(SOCKET0, SOCRXREADPOINT1, (read5100(SOCKET0, SOCRXRECSIZE1) + (size >> 8)));
		write5100(SOCKET0, SOCRXREADPOINT2, (read5100(SOCKET0, SOCRXRECSIZE2) + (size & 0xFF)));
		
		hwlib::cout << "[+] Read pointer increased by " << size << hwlib::endl;*/
		
		hwlib::cout << "[&] Increasing read pointer by: " << size << hwlib::endl;
		uint8_t currVal1, currVal2;
		currVal1 = read5100(SOCKET0, SOCRXREADPOINT1);
		currVal2 = read5100(SOCKET0, SOCRXREADPOINT2);
		hwlib::cout << currVal1 << " " << currVal2 << hwlib::endl;
		
		currVal1 += size >> 8;
		currVal2 += size & 0xFF;
		
		hwlib::cout << currVal1 << " " << currVal2 << hwlib::endl;
		
		write5100(SOCKET0, SOCRXREADPOINT1, currVal1);
		write5100(SOCKET0, SOCRXREADPOINT2, currVal2);
		
		if(read5100(SOCKET0, SOCRXREADPOINT1) == currVal1 && read5100(SOCKET0, SOCRXREADPOINT2) == currVal2)
			hwlib::cout << "[&] Increased read pointer verified" << hwlib::endl;
		
		hwlib::cout << "[+] Read pointer increased by " << size << hwlib::endl;
	}
	
	void increaseWritePointer(uint16_t size) {
		hwlib::cout << "[&] Increasing write pointer by: " << size << hwlib::endl;
		uint8_t currVal1, currVal2;
		currVal1 = read5100(SOCKET0, SOCTXWRITEPOINT1);
		currVal2 = read5100(SOCKET0, SOCTXWRITEPOINT2);
		hwlib::cout << currVal1 << " " << currVal2 << hwlib::endl;
		
		currVal1 += size >> 8;
		currVal2 += size & 0xFF;
		
		hwlib::cout << currVal1 << " " << currVal2 << hwlib::endl;
		
		write5100(SOCKET0, SOCTXWRITEPOINT1, currVal1);
		write5100(SOCKET0, SOCTXWRITEPOINT2, currVal2);
		
		if(read5100(SOCKET0, SOCTXWRITEPOINT1) == currVal1 && read5100(SOCKET0, SOCTXWRITEPOINT2) == currVal2)
			hwlib::cout << "[&] Increased write pointer verified" << hwlib::endl;
		
		hwlib::cout << "[+] Write pointer increased by " << size << hwlib::endl;
	}
	
	uint16_t getRXSize() {
		return concatBytes(read5100(SOCKET0, SOCRXRECSIZE1), read5100(SOCKET0, SOCRXRECSIZE2));
	}
	
	uint16_t getReadPointer() {
		return concatBytes(read5100(SOCKET0, SOCRXREADPOINT1), read5100(SOCKET0, SOCRXREADPOINT2));
	}
public:
	EthernetShield();
	
	bool testSourceIPAddress() {
		if (read5100(COMMON_REGS, SIPADREG1) == baseSourceIPAddress[0])
			return true;
		else
			return false;
	}
	
	bool testInterruptMaskSoc() {
		if (read5100(COMMON_REGS, IMRREG) == 0xEF)
			return true;
		else 
			return false;
	}
	
	void startServer();
	void runServer();
	void waitForConnection();
	void waitForReceivedData();
	void receiveData(volatile uint8_t* dst);
	void sendData(uint8_t *data, uint16_t len);
};

#endif // ETHERNETSHIELD_HPP
