#include "EthernetShield.hpp"
#include "hwlib.hpp"

void blink( hwlib::pin_in_out & led ){
   led.direction_set_output();
   while(1){
      led.set( 1 );
      hwlib::wait_ms( 200 );
      led.set( 0 );
      hwlib::wait_ms( 200 );      
   }
}

EthernetShield::EthernetShield()
{
	writeGatewayAddress();
	writeSourceHWAddress();
	writeSourceIPAddress();
	writeSubnetMask();
	enableInterrupt();
	setupRXMem();
	setupTXMem();
	
	hwlib::cout << "[+] Constructor setup done" << hwlib::endl;
}

void EthernetShield::startServer() {
	write5100(SOCKET0, MODE, 0x01); ///< Setup TCP mode
	hwlib::cout << "[+] TCP mode set" << hwlib::endl;
	
	write5100(SOCKET0, SOCSPORT1, 0x05);
	write5100(SOCKET0, SOCSPORT2, 0x39); ///< Setup TCP port 1337
	hwlib::cout << "[+] Port setup" << hwlib::endl;
	
	write5100(SOCKET0, SOCCOMMAND, OPEN); ///< Send Open to Command Reg
	hwlib::cout << "[+] Command register, command open" << hwlib::endl;
	
	write5100(SOCKET0, SOCCOMMAND, LISTEN); ///< Send Listen Command Reg
	hwlib::cout << "[+] Command register, command listen" << hwlib::endl;
}

void EthernetShield::runServer() {
	waitForConnection();
	
	while(1) {
		waitForReceivedData(); //TODO Connection not yet made
		receiveData(recvBuffer);
		
		hwlib::cout << "[&] Received data dump " <<
		hwlib::hex << recvBuffer[0] <<
		hwlib::hex << recvBuffer[1] <<
		hwlib::hex << recvBuffer[2] <<
		hwlib::hex << recvBuffer[3] <<
		hwlib::hex << recvBuffer[4] << hwlib::endl;
		hwlib::cout << "Dump 2 " << hwlib::hex << recvBuffer[1] << hwlib::endl;
		
		sendData(recvBuffer, 1);
		
		if ((read5100(SOCKET0, SOCIR) & DISCONNECTED) == DISCONNECTED) {
			write5100(SOCKET0, SOCCOMMAND, CLOSE);
			write5100(SOCKET0, SOCIR, 0x00);
		} 
		if ((read5100(SOCKET0, SOCIR) & TIMEOUT) == TIMEOUT) {
			write5100(SOCKET0, SOCCOMMAND, CLOSE);
			write5100(SOCKET0, SOCIR, 0x00);
		}
		
		write5100(SOCKET0, SOCCOMMAND, CLOSE);
		write5100(SOCKET0, SOCIR, 0x00);
		if(recvBuffer[0] == 0x53 && recvBuffer[1] == 0x54 && recvBuffer[2] == 0x4F && recvBuffer[3] == 0x56 && recvBuffer[4] == 0x45) {
				using namespace due;
				namespace target = hwlib::target;
				auto cs = target::pin_out(target::pins::d10);
				auto led = hwlib::target::pin_in_out( 1, 27 );
				blink(led);
		}
	}
}

void EthernetShield::receiveData(volatile uint8_t* dst) {
	RXRECVSIZE = getRXSize();
	uint16_t readPointer = getReadPointer();
	uint16_t getOffset = readPointer & SOCKETMASK;
	uint16_t size;
	uint16_t socBase = 0x6000;
	uint16_t startAddress = socBase + getOffset;
	
	hwlib::cout << "[-] Receive data setup done: " 
	<< hwlib::hex << RXRECVSIZE
	<< hwlib::hex << readPointer
	<< hwlib::hex << getOffset
	<< hwlib::hex << socBase
	<< hwlib::hex << startAddress
	<< hwlib::endl;
	
	if((getOffset + RXRECVSIZE) > SOCKETMASK) {
		size = SOCKETMASK - getOffset;
		largeRead(readPointer, size, (uint8_t *)dst);
		dst += size;
		largeRead(socBase, RXRECVSIZE - size, (uint8_t *)dst);
	} else {
		largeRead(startAddress, RXRECVSIZE, (uint8_t *)dst);
	}
	
	hwlib::cout << "[+] Data received" << hwlib::endl;
	
	increaseReadPointer(RXRECVSIZE);
	
	write5100(SOCKET0, SOCCOMMAND, RECV);
	hwlib::cout << "[+] Command register, command received" << hwlib::endl;
}

void EthernetShield::sendData(uint8_t *data, uint16_t len) {
	uint16_t sendPointer = concatBytes(read5100(SOCKET0, SOCTXWRITEPOINT1), read5100(SOCKET0, SOCTXWRITEPOINT2)); ///< Get pointer
	//sendPointer
	uint16_t offset = sendPointer & SOCKETMASK; ///< 
	uint16_t addr = offset + 0x4000;
	
	hwlib::cout << "[-] Send Data setup done:" << hwlib::endl;
	hwlib::cout << hwlib::hex << sendPointer << " " << 
	hwlib::hex << offset << " " << hwlib::hex << addr 
	<< " " << hwlib::hex << len << hwlib::endl;
	
	if ((offset + len) > SOCKETMASK) { ///< Ringbuffer structure
		uint16_t size = SOCKETMASK - offset;
		largeWrite(addr, size, data);
		largeWrite(TXMEM, len - size, data + size);
	} else {
		largeWrite(TXMEM, len, data);
	}
	
	increaseWritePointer(len);
	
	write5100(SOCKET0, SOCCOMMAND, SEND);
}

void EthernetShield::waitForConnection() {
	while((read5100(SOCKET0, SOCIR) & CONNECTED) != CONNECTED) {
		hwlib::wait_ns(20);
	}
	hwlib::cout << "[*] Connected interrupt received" << hwlib::endl;
	
	write5100(SOCKET0, SOCIR, 0x00);
}

void EthernetShield::waitForReceivedData() {
	while((read5100(SOCKET0, SOCIR) & RECEIVED) != RECEIVED) {
		hwlib::wait_ns(20);
	}
	hwlib::cout << "[*] Received data interrupt received" << hwlib::endl;
	
	write5100(SOCKET0, SOCIR, 0x00);
}
