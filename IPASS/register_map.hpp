#ifndef REGISTER_MAP_HPP
#define REGISTER_MAP_HPP

enum command : uint8_t {
	WRITE 			= 0xF0,
	READ 			= 0x0F,
};
	
enum memory_map : uint8_t {
	COMMON_REGS		= 0x00,
	SOCKET0			= 0x04,
	SOCKET1			= 0x05,
	SOCKET2			= 0x06,
	SOCKET3			= 0x07,
	TXMEM			= 0x40,
	RXMEM			= 0x60,
};

enum common_register_offset : uint8_t {
	MODEREG			= 0x00, ///< Mode Register
	GWIPREG1		= 0x01, ///< Gateway IP Add reg e.g 192
	GWIPREG2		= 0x02, ///< Gateway IP Add reg e.g 168
	GWIPREG3		= 0x03, ///< Gateway IP Add reg e.g 0
	GWIPREG4		= 0x04, ///< Gateway IP Add reg e.g 1
	SUBNMREG1		= 0x05, ///< Subnet Mask reg e.g 255
	SUBNMREG2		= 0x06, ///< Subnet Mask reg e.g 255
	SUBNMREG3		= 0x07, ///< Subnet Mask reg e.g 255
	SUBNMREG4		= 0x08, ///< Subnet Mask reg e.g 0
	SHWADREG1		= 0x09, ///< Source HW address e.g 00
	SHWADREG2		= 0x0A, ///< Source HW address e.g 08
	SHWADREG3		= 0x0B, ///< Source HW address e.g DC
	SHWADREG4		= 0x0C, ///< Source HW address e.g 01
	SHWADREG5		= 0x0D, ///< Source HW address e.g 02
	SHWADREG6		= 0x0E, ///< Source HW address e.g 03
	SIPADREG1		= 0x0F, ///< Source IP address e.g 192
	SIPADREG2		= 0x10, ///< Source IP address e.g 168
	SIPADREG3		= 0x11, ///< Source IP address e.g 1
	SIPADREG4		= 0x12, ///< Source IP address e.g 42
	IRREG			= 0x15, ///< Interrupt register
	IMRREG			= 0x16, ///< Interrupt mask register
	RTRREG			= 0x17, ///< Retry Time-value register
	RCRREG			= 0x19, ///< Retry Count Register
	RXMEMSIZEREG	= 0x1A, ///< Assign 8K Total To Sockets Rx
	TXMEMSIZEREG	= 0x1B, ///< Assign 8K Total To Sockets Tx
	PATRREG			= 0x1C, ///< Authentication Type in PPPoE Mode
	PTIMERREG		= 0x28, ///< PPP Link Control Request Timer
	PMAGIC			= 0x29, ///< PPP Link Control Magic Number
	UIPREG			= 0x2A, ///< Unreachable IP Add Reg
	UPORTREG		= 0x2E, ///< Unreachable Port Reg
};

enum socket_register_offset : uint8_t {
	MODE 			= 0x00, ///< Socket Mode
	SOCCOMMAND		= 0x01, ///< Socket Command
	SOCIR			= 0x02, ///< Socket Interrupt
	SOCSTATUS		= 0x03, ///< Socket Status
	SOCSPORT1		= 0x04, ///< Socket Source Port 1
	SOCSPORT2		= 0x05, ///< Socket Source Port 2
	SOCDESTMAC1		= 0x06, ///< Socket Destination HW Address 1
	SOCDESTMAC2		= 0x07, ///< Socket Destination HW Address 2
	SOCDESTMAC3		= 0x08, ///< Socket Destination HW Address 3
	SOCDESTMAC4		= 0x09, ///< Socket Destination HW Address 4
	SOCDESTMAC5		= 0x0A, ///< Socket Destination HW Address 5
	SOCDESTMAC6		= 0x0B, ///< Socket Destination HW Address 6
	SOCDESTIP1		= 0x0C, ///< Socket Destination IP Address 1
	SOCDESTIP2		= 0x0D, ///< Socket Destination IP Address 2
	SOCDESTIP3		= 0x0E, ///< Socket Destination IP Address 3
	SOCDESTIP4		= 0x0F, ///< Socket Destination IP Address 4
	SOCDESTPORT1	= 0x10, ///< Socket Destination Port 1
	SOCDESTPORT2	= 0x11, ///< Socket Destination Port 2
	SOCMAXSEG1		= 0x12, ///< Socket Maximum Segment Size 1
	SOCMAXSEG2		= 0x13, ///< Socket Maximum Segment Size 2
	SOCPROTRAW		= 0x14, ///< Socket Protocol IP Raw Mode
	SOCIPTOS		= 0x15, ///< Socket IP TOS
	SOCIPTTL		= 0x15, ///< Socket IP TTL
	SOCTXFREESIZE1	= 0x20, ///< Socket TX Free Size 1
	SOCTXFREESIZE2	= 0x21, ///< Socket TX Free Size 2
	SOCTXREADPOINT1	= 0x22, ///< Socket TX Read Pointer 1
	SOCTXREADPOINT2	= 0x23, ///< Socket TX Read Pointer 2
	SOCTXWRITEPOINT1= 0x24, ///< Socket TX Write Pointer 1
	SOCTXWRITEPOINT2= 0x25, ///< Socket TX Write Pointer 2
	SOCRXRECSIZE1	= 0x26, ///< Socket RX Received Size 1
	SOCRXRECSIZE2	= 0x27, ///< Socket RX Received Size 2
	SOCRXREADPOINT1	= 0x28, ///< Socket RX Read Pointer 1
	SOCRXREADPOINT2 = 0x29, ///< Socket RX Read Pointer 2
};

enum socket_interrupt : uint8_t {
	CONNECTED		= 0x01, ///< And Value for CON bit
	DISCONNECTED	= 0x02, ///< And Value for DISCON bit
	RECEIVED		= 0x04, ///< And Value for RECV bit
	TIMEOUT			= 0x08, ///< And Value for TIMEOUT bit
	SENDOK			= 0x10, ///< And Value for SEND_OK bit
};

enum socket_commands : uint8_t {
	OPEN			= 0x01, ///< Open Command
	LISTEN			= 0x02, ///< Listen Command
	CONNECT			= 0x04, ///< Connect Command
	DISCONNECT		= 0x08, ///< Disconnect Command
	CLOSE			= 0x10, ///< Close Command
	SEND			= 0x20, ///< Send Command
	SENDMAC			= 0x21, ///< Send Mac Command
	SENDKEEP		= 0x22, ///< Send Keep Command
	RECV			= 0x40, ///< Received Command
};

#endif // REGISTER_MAP_HPP