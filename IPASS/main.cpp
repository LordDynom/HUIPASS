//#include <iostream>

#include "hwlib.hpp"
#include "due-spi.hpp"
#include "EthernetShield.hpp"

// write| 0xF0	| Address Field	2 Byte	| Data Field 1 Byte
// read	| 0x0F	| Address Field 2 Byte	| Data Field 1 Byte

/*void blink( hwlib::pin_in_out & led ){
   led.direction_set_output();
   while(1){
      led.set( 1 );
      hwlib::wait_ms( 200 );
      led.set( 0 );
      hwlib::wait_ms( 200 );      
   }
}*/

int main( void ){
	WDT->WDT_MR = WDT_MR_WDDIS;
   
	/*using namespace due;
	namespace target = hwlib::target;
	
	auto cs = target::pin_out(target::pins::d10);
	auto led = hwlib::target::pin_in_out( 1, 27 );*/
	
	EthernetShield shield;
	
	shield.startServer();
	shield.runServer();
   
	/*if (shield.testInterruptMaskSoc()) {
		blink( led );
	}*/
}
