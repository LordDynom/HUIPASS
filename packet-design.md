|---------------|
|BINARY HEADER	|
|Bin info-	|
|Magic numbers-	|
|first offset-	|
|---------------|
|HEADER HEADER	|<-- Loader key -> hardcoded encrypted key on rom.
|Offsets to	|<-- Hardcoded key for signature, Metadata and other offsets
|other headers-	|
|Binary loader	|
|key-		|
|---------------|
|METADATA	|
|Keys+hash	|
|for section and|
|program headers|
|---------------|
|SIGNATURE	|
