#!/usr/bin/env python

import socket
import numpy as np
import gzip

SUBFOLDER = "./Images"
rawCounter = 0
compressionRatio = 0
bufferList = []

def average(source):
        tempList = []
        for x in source:
                tempList.append(len(x))

        return sum(tempList)/len(tempList)

def lreCompressor(source):
        width, height = source.shape
        buffer = b""
        for y in range(0, height):
                counter = 1
                for x in range(1, width):
                        if(source[x,y] != source[x-1,y]):
                                buffer += (counter).to_bytes(1, byteorder='big') + (source[x-1, y].item()).to_bytes(1, byteorder='big')
                                counter = 1
                        elif (source[x,y] == source[x-1,y]):
                                counter += 1
        return buffer

for i in range(0, 6563):
        tempBuffer = b""
        destBuffer = np.loadtxt(SUBFOLDER+'/rawFrame'+str(i)+'.txt', delimiter=',')
        destBuffer = destBuffer.astype(int)
        width, height = destBuffer.shape
        for y in range(0,height):
                for x in range(0,width):
                    tempBuffer += str(destBuffer[x,y].item()).encode()
		
        bufferList.append(tempBuffer)

rawCounter = average(bufferList)
print("Raw frame average byte count: ", rawCounter)
rawCounter = 0
bufferList = []

for i in range(0, 6563):
        tempBuffer = b""
        destBuffer = np.loadtxt(SUBFOLDER+'/rawFrame'+str(i)+'.txt', delimiter=',')
        destBuffer = destBuffer.astype(int)
        compressed = lreCompressor(destBuffer)
		
        bufferList.append(compressed)

rawCounter = average(bufferList)
print("Raw frame average byte count: ", rawCounter)
rawCounter = 0
bufferList
	
for i in range(0, 6563):
        tempBuffer = b""
        destBuffer = np.loadtxt(SUBFOLDER+'/rawFrame'+str(i)+'.txt', delimiter=',')
        destBuffer = destBuffer.astype(int)
        compressed = lreCompressor(destBuffer)
	
        extraCompressed = gzip.compress(compressed)
	
        bufferList.append(extraCompressed)

rawCounter = average(bufferList)
print("Raw frame average byte count: ", rawCounter)
