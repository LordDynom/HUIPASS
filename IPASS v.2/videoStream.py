#!/usr/bin/env python

import pickle
import socket
import time
import numpy as np

def rleCompressor(source):
	width, height = source.shape
	tempDestination = []
	destination = b""
	trigger = 0
	count = 1
	lst = []
	prev = source[0,0]
	for y in range(0, height):
		for x in range(0, width):
			#print(x, " ", count)
			if(source[x,y] != prev or trigger == 1):
				#print("Triggerd on", count)
				if (x == 0):
					start = (128-(count-1)).to_bytes(1, byteorder='big')
					row = (y-1).to_bytes(1, byteorder='big')
				else:
					start = (x+1-count).to_bytes(1, byteorder='big')
					row = (y).to_bytes(1, byteorder='big')
				amount = (count).to_bytes(1, byteorder='big')
				destinationCharacter = (source[x,y].item()).to_bytes(1, byteorder='big')


				entry = [row, start, amount, prev.astype(int)]
				lst.append(entry)
				count = 1
				trigger = 0
				prev = source[x,y]
			else:
				count += 1
				if (x == 127):
					trigger = 1

	if(int.from_bytes(lst[0][2], byteorder='big') > 128):
		temp = int.from_bytes(lst[0][2], byteorder='big') - 1
		lst[0][2] = temp.to_bytes(1, byteorder='big')

	return lst

#
TCP_IP = '192.168.1.142'
TCP_PORT = 1337
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
print("connected")

SUBFOLDER = "./Images"
close = b'3'
counter = 1

for i in range(321, 6563):
    tempBuffer = b""
    destBuffer = np.loadtxt(SUBFOLDER+'/rawFrame'+str(i)+'.txt', delimiter=',')
    destBuffer = destBuffer.astype(int)
    tempBuffer = rleCompressor(destBuffer)
    print("Writing frame:", counter)
    for p in tempBuffer:
        recvBuffer = []
        s.send(b''.join(p))
        recvBuffer = s.recv(1)
        while(recvBuffer[0] != 0x32):
            time.sleep(1)

    counter += 1


s.send(close)
s.close()
