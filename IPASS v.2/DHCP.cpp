#include "DHCP.hpp"

DHCP::DHCP(bool debug):
	UDP(debug)
{
}

DHCP::~DHCP()
{
}

void DHCPMessage(uint8_t type) {
	/*uint8_t buffer
	
	typedef struct _RIP_MSG {
		uint8_t op; 
		uint8_t htype; 
		uint8_t hlen;
		uint8_t hops;
		u_long  xid;
		u_int   secs;
		u_int   flags;
		uint8_t ciaddr[4];
		uint8_t yiaddr[4];
		uint8_t siaddr[4];
		uint8_t giaddr[4];
		uint8_t chaddr[16];
		uint8_t sname[64];
		uint8_t file[128];
		uint8_t OPT[312];
	}RIP_MSG;
	
	//uint8_t GET_SN_MASK[4];						/**< Subnet mask received from the DHCP server */
	//uint8_t GET_GW_IP[4];						/**< Gateway ip address received from the DHCP server */
	//uint8_t GET_SIP[4] = {0, 0, 0, 0};			/**< Local ip address received from the DHCP server */
	//
	//static u_char DHCP_SIP[4] = {0, 0, 0, 0};     /**< DHCP server ip address is discovered */
	//
	//static u_long DHCP_XID = 0x75269875;
	//static RIP_MSG* pRIPMSG;					/**< Pointer for the DHCP message */
	//RIP_MSG RIPMSG;								/**< Structure definition */
	//
	//uint16_t recv_msg_size;
	//uint8_t *recv_msg_end;
	//uint8_t *current_option;
	//uint8_t  option_length;
	//
	//u_char ip[4];
	//u_int i=0;
	//uint16_t port;
	
	
	/************************************************************************/
	/*                           Send DHCPDISCOVER                          */
	/************************************************************************/
	/*
	pRIPMSG = &RIPMSG;
	
	memset((void*)pRIPMSG, 0, sizeof(RIP_MSG));     // Set whole of RIPMSG to 0
	
	pRIPMSG->op = DHCP_BOOTREQUEST;
	pRIPMSG->htype = DHCP_HTYPE10MB;
	pRIPMSG->hlen = DHCP_HLENETHERNET;
	pRIPMSG->hops = DHCP_HOPS;
	pRIPMSG->xid = DHCP_XID;
	pRIPMSG->secs = DHCP_SECS;
	pRIPMSG->flags = DHCP_FLAGSBROADCAST;
	pRIPMSG->chaddr[0] = baseSourceHWAddress[0];
	pRIPMSG->chaddr[1] = baseSourceHWAddress[1];
	pRIPMSG->chaddr[2] = baseSourceHWAddress[2];
	pRIPMSG->chaddr[3] = baseSourceHWAddress[3];
	pRIPMSG->chaddr[4] = baseSourceHWAddress[4];
	pRIPMSG->chaddr[5] = baseSourceHWAddress[5];
	
	/* MAGIC_COOKIE */
	/*pRIPMSG->OPT[i++] = (char)((MAGIC_COOKIE >> 24)& 0xFF);
	pRIPMSG->OPT[i++] = (char)((MAGIC_COOKIE >> 16)& 0xFF);
	pRIPMSG->OPT[i++] = (char)((MAGIC_COOKIE >> 8)& 0xFF);
	pRIPMSG->OPT[i++] = (char)(MAGIC_COOKIE& 0xFF);*/
	
	/* Option Request Param. */
/*	pRIPMSG->OPT[i++] = dhcpMessageType;
	pRIPMSG->OPT[i++] = 0x01;
	pRIPMSG->OPT[i++] = DHCP_DISCOVER;*/
	
	/*// Client identifier
	pRIPMSG->OPT[i++] = dhcpClientIdentifier;
	pRIPMSG->OPT[i++] = 0x07;
	pRIPMSG->OPT[i++] = 0x01;
	pRIPMSG->OPT[i++] = baseSourceHWAddress[0];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[1];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[2];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[3];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[4];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[5];
	
	// host name
	pRIPMSG->OPT[i++] = hostName;
	pRIPMSG->OPT[i++] = strlen(HOST_NAME)+3; // length of hostname + 3
	strcpy((char*)&(pRIPMSG->OPT[i]),HOST_NAME);
	
	i+=strlen(HOST_NAME);
	
	pRIPMSG->OPT[i++] = baseSourceHWAddress[3];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[4];
	pRIPMSG->OPT[i++] = baseSourceHWAddress[5];
	
	// IP Address Lease Time request
	pRIPMSG->OPT[i++] = dhcpIPaddrLeaseTime;
	pRIPMSG->OPT[i++] = 4;
	pRIPMSG->OPT[i++] = 0xFF;
	pRIPMSG->OPT[i++] = 0xFF;
	pRIPMSG->OPT[i++] = 0xFF;
	pRIPMSG->OPT[i++] = 0xFF;
	
	// Parameter Request List
	pRIPMSG->OPT[i++] = dhcpParamRequest;
	pRIPMSG->OPT[i++] = 0x02;
	pRIPMSG->OPT[i++] = subnetMask;
	pRIPMSG->OPT[i++] = routersOnSubnet;
	pRIPMSG->OPT[i++] = endOption;
	
	memcpy(frame, (void*)pRIPMSG, sizeof(frame));
	
	sendData()*/
	
	uint8_t buffer[32] = {0}; ///< Each row of a frame is 4 octecs wide
	
	buffer[0] = DHCPBootRequest;
	buffer[1] = 1; ///< HTYPE Ethernet 10Mb
	buffer[2] = 6; ///< MAC addr length
	buffer[3] = 0; ///< DHCP hops
	
	/*
	fe a6 a1 f4 
	
	Random XID, chosen by a fair diceroll. Garanteed to be random.
	*/
	buffer[4] = 0xFE;
	buffer[5] = 0xA6;
	buffer[6] = 0xA1;
	buffer[7] = 0xF4;
	
	/*
	Seconds elapsed. This field isn't really used anymore, but has to be included.
	The Arduino is quite slow, so we are going to set it to 8 seconds. Could later
	be fixed using the onboard RTC time. But we don't have an interface for that now.
	*/
	buffer[8] = 0x00;
	buffer[9] = 0x08;
	
	//Flags
	buffer[10] = 0x80;
	buffer[11] = 0x00;
	
	setDataInTXBuffer(buffer, 28);
	clearBuffer(buffer);
	
	memcopy(buffer, baseSourceHWAddress, 6);
	
	setDataInTXBuffer(buffer, 16);
	clearBuffer(buffer);
	
	//Server Name or SName. Just leave this 0
	for(uint8_t i = 0; i < 6 ++i) {
		setDataInTXBuffer(buffer, 32);
	}
	
	//Options Field
	//Magic cookie - WTF?
	buffer[0] = 0x63;
	buffer[1] = 0x82;
	buffer[2] = 0x53;
	buffer[3] = 0x63;
	
	//Message Type field
	buffer[4] = 0x35;
	buffer[5] = 0x01;
	buffer[6] = type;
	
	//Client identifier
	buffer[7] = 0x3D;
	buffer[8] = 0x07;
	buffer[9] = 0x01;
	
	for(uint8_t i = 0; i < 6; ++i)
		buffer[10 + i] = baseSourceHWAddress[i];
	
	//Host name
	buffer[16] = 12;
	buffer[17] = strlen(HOST_NAME) + 6;
	strcpy((char*)&(buffer[18]), HOST_NAME);
	
	printByte((char*)&(buffer[24]), baseSourceHWAddress[3]);
	printByte((char*)&(buffer[26]), baseSourceHWAddress[4]);
	printByte((char*)&(buffer[28]), baseSourceHWAddress[5]);
	
	//put data in W5100 transmit buffer
	setDataInTXBuffer(buffer, 30);
	
/*	if(messageType == DHCP_REQUEST)
	{
		buffer[0] = dhcpRequestedIPaddr;
		buffer[1] = 0x04;
		buffer[2] = _dhcpLocalIp[0];
		buffer[3] = _dhcpLocalIp[1];
		buffer[4] = _dhcpLocalIp[2];
		buffer[5] = _dhcpLocalIp[3];
	
		buffer[6] = dhcpServerIdentifier;
		buffer[7] = 0x04;
		buffer[8] = _dhcpDhcpServerIp[0];
		buffer[9] = _dhcpDhcpServerIp[1];
		buffer[10] = _dhcpDhcpServerIp[2];
		buffer[11] = _dhcpDhcpServerIp[3];
	
		//put data in W5100 transmit buffer
		_dhcpUdpSocket.write(buffer, 12);
	}*/
	
	buffer[0] = dhcpParamRequest;
	buffer[1] = 0x06;
	buffer[2] = subnetMask;
	buffer[3] = routersOnSubnet;
	buffer[4] = dns;
	buffer[5] = domainName;
	buffer[6] = dhcpT1value;
	buffer[7] = dhcpT2value;
	buffer[8] = endOption;
	
	//put data in W5100 transmit buffer
	setDataInTXBuffer(buffer, 9);
	
	confirmSend(discoverAddr, DHCP_SERVER_PORT);

}

void DHCP::printByte(char * buf, uint8_t n ) {
  char *str = &buf[1];
  buf[0]='0';
  do {
    unsigned long m = n;
    n /= 16;
    char c = m - 16 * n;
    *str-- = c < 10 ? c + '0' : c + 'A' - 10;
  } while(n);
}