import numpy as np
import pickle

def rleCompressor(source):
    width, height = source.shape
    tempDestination = []
    destination = b""
    trigger = 0
    count = 1
    lst = []
    prev = source[0,0]
    for y in range(0, height):
        for x in range(0, width):
            if(source[x,y] != prev or trigger == 1):
                if (x == 0):
                    start = (128-(count-1)).to_bytes(1, byteorder='big')
                    row = (y-1).to_bytes(1, byteorder='big')
                else:
                    start = (x+1-count).to_bytes(1, byteorder='big')
                    row = (y).to_bytes(1, byteorder='big')
                amount = (count).to_bytes(1, byteorder='big')
                destinationCharacter = (source[x,y].item()).to_bytes(1, byteorder='big')


                entry = [row, start, amount, prev.astype(int)]
                lst.append(entry)
                count = 1
                trigger = 0
                prev = source[x,y]
        else:
            count += 1
            if (x == 127):
                trigger = 1

    if(int.from_bytes(lst[0][2], byteorder='big') > 128):
        temp = int.from_bytes(lst[0][2], byteorder='big') - 1
        lst[0][2] = temp.to_bytes(1, byteorder='big')

    return lst

SUBFOLDER = "./Images"

for i in range(0, 6563):
    destBuffer = np.loadtxt(SUBFOLDER+'/rawFrame'+str(i)+'.txt', delimiter=',')
    destBuffer = destBuffer.astype(int)
    tempBuffer = rleCompressor(destBuffer)
    pickle.dump(tempBuffer, open(SUBFOLDER+'/rawDumpFrame'+str(i)+'.p', 'wb'))

